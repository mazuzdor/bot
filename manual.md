# Bot Manual
Note: when IP is mentioned, it is referring to the position of the server in the channels' list of servers and not an IP address.

- [Admin Commands](#admin-commands)
    - [addquerychannel](#addquerychannel)
    - [removequerychannel](#removequerychannel)
    - [addserver](#addserver)
    - [delerver](#delserver)
    - [editserver](#editserver)
        - [addalias](#addalias)
        - [delalias](#delalias)
        - [setname](#setname)
        - [delname](#delname)
        - [setpassword](#setpassword)
        - [delpassword](#delpassword)
        - [seturl](#seturl)
        - [delurl](#delurl)
        - [updateaddress](#updateaddress)
- [Public Commands](#public-commands)
    - [inviteseabot](#inviteseabot)
    - [ip](#ip)
    - [server](#server)
    - [query](#query)

## Admin Commands
All admin commands requires the author to have the "Manage Guild" permission.

### addquerychannel
Initialize and make the bot respond to commands in the current channel.

`.addquerychannel`

### removequerychannel
Removes the current channel as a query channel. This will also delete all saved servers. Because this is a destructive command, the bot will give a warning message and ask for confirmation before removing it.

`.addquerychannel`

### addserver
Add your favorite servers to the current query channel by typing:

`.addserver url [optional description]`

Example usage:
- .addserver someutserver.tld
- .addserver unreal://someutserver:7777?password=serverpassword

As you can see in the second example, if the server you want to add has a password, you can add it here by providing a query string. If you want to add one later or update an existing one, see [setpassword](#setpassword).

If the optional description is omitted, the server's name will be used instead (this is not recommended as server names tend to be very long with added ascii art to make them more visible, and the bot will truncate the name at 40 characters.) If you want to set or change a server's name later, see [setname](#setname).

Note: UT servers uses a dedicated query port that normally has an offset of +1 from the game port, and this bot makes that assumption about the url. Should you need to add a server whose port uses a different offset, just provide a port number that has an offset -1 from the query port and the bot will retrieve the actual game port from the server and use that.

### delserver
Remove a server from the channel's list of servers by typing:

`.delserver <alias or IP>`

### editserver
The base command for editing servers. Can also be invoked using its alias "es".

`.editserver <alias or IP>  subcommand [optional arg]`

The following subcommands are available:

#### addalias
Assigns one or more aliases to the selected server. Aliases have the following restrictions:
- Can not be all numbers
- Can not start with 'ip'
- Can not contain any non-alphanumeric symbols

`.editserver <alias or IP> addalias alias1 alias2`

Aliases are case-insensitive.

#### delalias
Removes one or more aliases from the selected server.

`.editserver <alias or IP> delalias alias1 alias2`

#### setname
Set or update a custom name for the selected server which will be shown when using the [server](#server) command.

`.editserver <alias or IP> setname new name`

Names will be truncated at 40 characters.

#### delname
Deletes the selected server's given name. The server's hostname will be used instead.

`.editserver <alias or IP> delname`

#### setpassword
Set or update a server's password.

`.editserver <alias or IP> setpassword hunter2`

#### delpassword
Delete a server's password

`.editserver <alias or IP> delpassword`

#### seturl

Set a server's URL by using the seturl command. This will make the server's query embed title point to the given URL, and is useful if the server has a homepage.

`.editserver 1 seturl http(s)://servers_website.com`

#### delurl

Remove a server's URL/homepage.

`.editserver 1 delurl`

#### updateaddress
Update the address for a server,

`.editserver 1 updateaddress unreal://host:port`

## Public Commands
### inviteseabot
Invite seabot to your own guild.

`.inviteseabot`

Seabot will send a Direct Message with an invite link. If you don't receive anything, make sure that you allow Direct Messages in your Discord settings.

### ip
Post a server's address to a channel.

`.ip <alias or IP>`

### server
This command will query all the servers in the channel's list of servers and display them in a list showing the name of the server (or hostname if the name isn't set) and the number of players currently on the servers. The bot will wait 500ms for the results and output an embed with the results it has. If some queries are still pending, it will wait up to an additional 3.5 seconds for the missing results and update the embed where needed.

`.<server|servers>`

### query
This command will query a UT server by either IP or alias, or any URL and display the results in an embed. 

`.<query|q> <alias, IP or url>`

Note: you might see that the number of players and the number of player names some times doesn't match. This is due to a bug in UT where if the server admin chooses to display the spectators, every spectator shown will remove a player from the output.